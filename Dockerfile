FROM ubuntu:18.04

LABEL maintainer="loodos.com"

ENV DEBIAN_FRONTEND=noninteractive \
  TERM=xterm \
  ANDROID_SDK_URL="https://dl.google.com/android/repository/tools_r25.2.5-linux.zip" \
  ANDROID_BUILD_TOOLS_VERSION=27.0.0 \
  #android-24,android-25,android-26
  ANDROID_APIS="android-26" \
  ANT_HOME="/usr/share/ant" \
  MAVEN_HOME="/usr/share/maven" \
  GRADLE_HOME="/usr/share/gradle" \
  ANDROID_HOME="/opt/android" \
  GRADLE_VERSION=4.1 \
  NODEJS_VERSION=8.11.1 \
  CORDOVA_VERSION=8.1.2


# Install basics
RUN set -x && \
  apt-get update && \
  apt-get install -y git bzip2 openssh-client wget curl unzip build-essential --no-install-recommends && \
  apt-get install -y --reinstall ca-certificates

# Adding unicode support
RUN apt-get install -y locales
RUN locale-gen en_US.UTF-8
ENV LANG='en_US.UTF-8' LANGUAGE='en_US:en' LC_ALL='en_US.UTF-8'

# Install JAVA
RUN set -x && \
  apt-get install -y software-properties-common --no-install-recommends

RUN set -x && \
  ## use WebUpd8 PPA
  add-apt-repository ppa:webupd8team/java -y && \
  apt-get update && \
  ## automatically accept the Oracle license
  echo oracle-java8-installer shared/accepted-oracle-license-v1-1 select true | /usr/bin/debconf-set-selections && \
  apt-get install -y oracle-java8-installer && \
  apt-get install -y oracle-java8-set-default

ENV JAVA_HOME /usr/lib/jvm/java-8-oracle


# Install Android
ENV PATH $PATH:$ANDROID_HOME/tools:$ANDROID_HOME/platform-tools:$ANDROID_HOME/build-tools/$ANDROID_BUILD_TOOLS_VERSION:$ANT_HOME/bin:$MAVEN_HOME/bin

WORKDIR /opt

RUN dpkg --add-architecture i386 && \
  apt-get -qq update && \
  apt-get -qq install -y maven ant libncurses5:i386 libstdc++6:i386 zlib1g:i386 && \
  mkdir android && cd android && \
  wget -O tools.zip ${ANDROID_SDK_URL} && \
  unzip tools.zip && rm tools.zip && \
  echo y | android update sdk -a -u -t platform-tools,${ANDROID_APIS},build-tools-${ANDROID_BUILD_TOOLS_VERSION} && \
  chmod a+x -R $ANDROID_HOME && \
  chown -R root:root $ANDROID_HOME


# Install Gradle
ENV PATH=$PATH:$GRADLE_HOME/gradle-${GRADLE_VERSION}/bin

RUN mkdir $GRADLE_HOME && cd $GRADLE_HOME && \
  wget --output-document=gradle.zip --quiet https://services.gradle.org/distributions/gradle-"$GRADLE_VERSION"-bin.zip && \
  unzip -q gradle.zip && \
  rm -f gradle.zip && \
  chmod a+x -R $GRADLE_HOME && \
  chown -R root:root $GRADLE_HOME


# Install NodeJS
ENV PATH=$PATH:/opt/node/bin

WORKDIR /opt/node
RUN curl -sL https://nodejs.org/dist/v${NODEJS_VERSION}/node-v${NODEJS_VERSION}-linux-x64.tar.gz | tar xz --strip-components=1

# Install Cordova
WORKDIR /tmp
RUN npm i -g --unsafe-perm cordova@${CORDOVA_VERSION}    

# Install ionic
RUN npm i -g --unsafe-perm ionic@${IONIC_VERSION} && \
  ionic --no-interactive config set -g daemon.updates false

# Clean up
RUN rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/* && \
  apt-get purge -y --auto-remove software-properties-common && \
  apt-get autoremove -y && \
  apt-get clean
