﻿# ionic - docker image

## What is it

This repository contains Dockerfiles to create Docker images containing

- java 8
- android SDK, ant, maven, gradle
- npm, nodejs
- cordova
- ionic framework

base from Ubuntu.

## Usage

### Pull from registry

`docker pull registry.gitlab.com/loodos/docker-images/ionic:latest`

### Build manualy

`docker build -t registry.gitlab.com/loodos/docker-images/ionic:debug .`

### Use as base image

`FROM registry.gitlab.com/loodos/docker-images/ionic:latest`

## License

This library is release under Apache 2.0 License.

Copyright (c) 2018 [Loodos](https://loodos.com)

See [LICENSE](https://gitlab.com/loodos/docker-images/ionic/blob/master/LICENSE)

## Acknowledgements

Thanks for providing free open source licensing

- https://about.gitlab.com/2018/10/24/setting-up-gitlab-ci-for-android-projects/
- https://github.com/mindrunner/docker-android-sdk
- https://hub.docker.com/_/openjdk
- https://hub.docker.com/r/marcoturi/ionic/dockerfile
- 